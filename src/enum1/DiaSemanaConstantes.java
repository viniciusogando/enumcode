package enum1;

public class DiaSemanaConstantes {

    //Sempre declaramos final, valor constante [ SEMPRE EM CAIXA ALTA ] que nao pode mudar.
    public static final int SEGUNDA = 1;
    public static final int TERCA = 2;
    public static final int QUARTA = 3;
    public static final int QUINTA = 4;
    public static final int SEXTA = 5;
    public static final int SABADO = 6;
    public static final int DOMINGO = 7;
    //Classe com 7 constantes

}
